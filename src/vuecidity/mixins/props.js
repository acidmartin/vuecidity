const props = {
  props: {
    disabled: {
      type: Boolean,
      required: false,
      default: false
    },
    color: {
      type: String,
      required: false,
      default: 'grey'
    },
    dark: {
      type: Boolean,
      required: false,
      default: true
    },
    block: {
      type: Boolean,
      required: false,
      default: false
    }
  }
}

export default props
