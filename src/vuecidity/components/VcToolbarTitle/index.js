import VcToolbarTitle from './VcToolbarTitle'

VcToolbarTitle.install = function install (Vue) {
  Vue.component(VcToolbarTitle.name, VcToolbarTitle)
}

export default VcToolbarTitle
