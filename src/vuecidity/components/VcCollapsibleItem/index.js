import VcCollapsibleItem from './VcCollapsibleItem'

VcCollapsibleItem.install = function install (Vue) {
  Vue.component(VcCollapsibleItem.name, VcCollapsibleItem)
}

export default VcCollapsibleItem
