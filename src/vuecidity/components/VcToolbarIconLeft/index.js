import VcToolbarIconLeft from './VcToolbarIconLeft'

VcToolbarIconLeft.install = function install (Vue) {
  Vue.component(VcToolbarIconLeft.name, VcToolbarIconLeft)
}

export default VcToolbarIconLeft
