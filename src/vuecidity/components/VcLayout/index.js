import VcLayout from './VcLayout'

VcLayout.install = function install (Vue) {
  Vue.component(VcLayout.name, VcLayout)
}

export default VcLayout
