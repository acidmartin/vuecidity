import VcFooter from './VcFooter'

VcFooter.install = function install (Vue) {
  Vue.component(VcFooter.name, VcFooter)
}

export default VcFooter
