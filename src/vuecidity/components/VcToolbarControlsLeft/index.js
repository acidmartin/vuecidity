import VcToolbarControlsLeft from './VcToolbarControlsLeft'

VcToolbarControlsLeft.install = function install (Vue) {
  Vue.component(VcToolbarControlsLeft.name, VcToolbarControlsLeft)
}

export default VcToolbarControlsLeft
