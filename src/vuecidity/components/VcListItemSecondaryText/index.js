import VcListItemSecondaryText from './VcListItemSecondaryText'

VcListItemSecondaryText.install = function install (Vue) {
  Vue.component(VcListItemSecondaryText.name, VcListItemSecondaryText)
}

export default VcListItemSecondaryText
