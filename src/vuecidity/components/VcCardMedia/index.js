import VcCardMedia from './VcCardMedia'

VcCardMedia.install = function install (Vue) {
  Vue.component(VcCardMedia.name, VcCardMedia)
}

export default VcCardMedia
