import VcToolbar from './VcToolbar'

VcToolbar.install = function install (Vue) {
  Vue.component(VcToolbar.name, VcToolbar)
}

export default VcToolbar
