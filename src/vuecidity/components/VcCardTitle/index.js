import VcCardTitle from './VcCardTitle'

VcCardTitle.install = function install (Vue) {
  Vue.component(VcCardTitle.name, VcCardTitle)
}

export default VcCardTitle
