import VcButton from './VcButton'

VcButton.install = function install (Vue) {
  Vue.component(VcButton.name, VcButton)
}

export default VcButton
