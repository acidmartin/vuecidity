import VcDialogHeader from './VcDialogHeader'

VcDialogHeader.install = function install (Vue) {
  Vue.component(VcDialogHeader.name, VcDialogHeader)
}

export default VcDialogHeader
