import VcJumbotron from './VcJumbotron'

VcJumbotron.install = function install (Vue) {
  Vue.component(VcJumbotron.name, VcJumbotron)
}

export default VcJumbotron
