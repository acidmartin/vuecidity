import VcContainer from './VcContainer'

VcContainer.install = function install (Vue) {
  Vue.component(VcContainer.name, VcContainer)
}

export default VcContainer
