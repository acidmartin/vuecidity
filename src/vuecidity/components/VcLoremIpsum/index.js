import VcLoremIpsum from './VcLoremIpsum'

VcLoremIpsum.install = function install (Vue) {
  Vue.component(VcLoremIpsum.name, VcLoremIpsum)
}

export default VcLoremIpsum
