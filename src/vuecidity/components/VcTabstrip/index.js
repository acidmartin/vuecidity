import VcTabstrip from './VcTabstrip'

VcTabstrip.install = function install (Vue) {
  Vue.component(VcTabstrip.name, VcTabstrip)
}

export default VcTabstrip
