import VcSwitcher from './VcSwitcher'

VcSwitcher.install = function install (Vue) {
  Vue.component(VcSwitcher.name, VcSwitcher)
}

export default VcSwitcher
