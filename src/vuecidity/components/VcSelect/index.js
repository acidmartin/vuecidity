import VcSelect from './VcSelect'

VcSelect.install = function install (Vue) {
  Vue.component(VcSelect.name, VcSelect)
}

export default VcSelect
