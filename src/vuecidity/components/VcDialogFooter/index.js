import VcDialogFooter from './VcDialogFooter'

VcDialogFooter.install = function install (Vue) {
  Vue.component(VcDialogFooter.name, VcDialogFooter)
}

export default VcDialogFooter
