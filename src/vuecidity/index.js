import './vuecidity-content.css'
import './vuecidity-colors.css'
import './vuecidity-font-colors.css'
import './vuecidity.css'
import './vuecidity-elevation.css'
import './vuecidity-elevation-up.css'
import './vuecidity-typography.css'
import './vuecidity-spacing.css'
import './vuecidity-css-ripple.css'
import './vuecidity-tooltip.css'
import * as components from './components'

function Vuecidity (Vue) {
  const Vuecidity = components.Vuecidity

  Vue.use(Vuecidity, {
    components
  })
}

export default Vuecidity
