#Vuecidity

Website: [http://vuecidity.wemakesites.net](http://vuecidity.wemakesites.net)
Repo: [https://bitbucket.org/acidmartin/vuecidity](https://bitbucket.org/acidmartin/vuecidity)

Author: [Martin Ivanov](https://wemakesites.net)

[Vuecidity](http://vuecidity.wemakesites.net) is a free, open source and licensed under MIT component 
library for the Vue.js framework, inspired by Google Material 
Design and Bootstrap. It provides developers with a huge set of 
30+ UI components, 24-column responsive layout grid system, 
styles and colors, beautifully crafted form elements and a 
couple of useful Vue directives, which allows them to concentrate 
on the functionality of the web, iOS, Android and desktop apps t
hey build. Powered by Vue, Vuecidity can be used to simultaneously 
develop amazing responsive websites, PWAs, desktop applications 
(through Electron) or mobile apps (through Cordova) using the 
same codebase written in ES6. SSR as well as many more components 
are on the roadmap and will be available in upcoming versions as 
Vuecidity is in constant development and improvement.

###Demos, documentation and quick start

http://vuecidity.wemakesites.net